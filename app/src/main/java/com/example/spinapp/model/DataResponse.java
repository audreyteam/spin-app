package com.example.spinapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataResponse {

    @SerializedName("data")
    private List<ImageData> data;

    public List<ImageData> getImageDataList() {
        return data;
    }
}
