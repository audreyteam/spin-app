package com.example.spinapp.model;

import com.google.gson.annotations.SerializedName;

public class ImageData {
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String url;

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }
}
