package com.example.spinapp.utils;

import java.util.List;

public class ListUtils {
    private static final int EMPTY_LIST_COUNT = 0;

    public static boolean isEmpty(List<?> list) {
        return list == null || list.isEmpty();
    }

    public static int size(List<?> list) {
        return isEmpty(list) ? EMPTY_LIST_COUNT : list.size();
    }
}
