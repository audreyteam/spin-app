package com.example.spinapp.utils;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Wrapper for {@link Scheduler}.
 *
 * <p>This allow us to easily replace scheduler with mock equivalent under test.</p>
 */
public class SchedulerProvider {

    /**
     * Returns a {@link Scheduler} that represent a Background scheduler.
     * <p> This shall be used to perform operation in background.</p>
     */
    public Scheduler io() {
        return Schedulers.io();
    }

    /**
     * Returns a {@link Scheduler} that represent the android MainThread.
     * <p> This shall be used to touch UI component only.</p>
     */
    public Scheduler mainThread() {
        return AndroidSchedulers.mainThread();
    }

}
