package com.example.spinapp.rest;

import com.example.spinapp.model.DataResponse;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Communicates with the servers, make queries to extract the required information.
 */
public interface DataRestApi {

    @GET("bins/n30si")
    Single<DataResponse> getImageDataList();
}
