package com.example.spinapp.ui;

import android.os.Bundle;

import com.example.spinapp.R;

import androidx.appcompat.app.AppCompatActivity;

public class ImageDataListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_list_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new ImageDataListFragment())
                    .commit();
        }
    }
}
