package com.example.spinapp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.spinapp.R;
import com.example.spinapp.adapter.ImageDataListAdapter;
import com.example.spinapp.utils.ListUtils;
import com.example.spinapp.viewmodel.ImageDataListViewModel;
import com.example.spinapp.viewmodel.ViewModelFactory;

import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ImageDataListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private final ViewModelFactory viewModelFactory = new ViewModelFactory();
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.empty_list_view)
    TextView emptyView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private ImageDataListAdapter listAdapter;
    private ImageDataListViewModel viewModel;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_list_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        listAdapter = new ImageDataListAdapter(getContext(), /* imageDataList= */ Collections.emptyList());
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.image_list_column_count)));

        viewModel = new ViewModelProvider(this, viewModelFactory).get(ImageDataListViewModel.class);
        fetchImageDataList(/* readFromCache= */true);
    }

    private void fetchImageDataList(boolean readFromCache) {
        viewModel.getImageDataList(readFromCache).observe(getViewLifecycleOwner(), dataList -> {
            setEmptyViewVisibility(ListUtils.isEmpty(dataList));
            swipeRefreshLayout.setRefreshing(false);
            listAdapter.setImageDataList(dataList);
        });
    }

    private void setEmptyViewVisibility(boolean isVisible) {
        if (isVisible) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        fetchImageDataList(/* readFromCache= */false);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
