package com.example.spinapp.viewmodel;

import com.example.spinapp.model.ImageData;
import com.example.spinapp.repo.DataRepository;
import com.example.spinapp.utils.SchedulerProvider;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public class ImageDataListViewModel extends ViewModel {
    private final MutableLiveData<List<ImageData>> imageDataListLiveData;
    private final DataRepository dataRepository;
    private final SchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;

    ImageDataListViewModel(DataRepository dataRepository, SchedulerProvider schedulerProvider) {
        this.dataRepository = dataRepository;
        this.schedulerProvider = schedulerProvider;
        this.compositeDisposable = new CompositeDisposable();
        imageDataListLiveData = new MutableLiveData<>();
    }

    public LiveData<List<ImageData>> getImageDataList(boolean readFromCache) {
        if (!readFromCache || imageDataListLiveData.getValue() == null) {
            fetchImageDataList();
        }
        return imageDataListLiveData;
    }

    private void fetchImageDataList() {
        compositeDisposable.add(dataRepository.fetchImageDataList().subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.mainThread())
                .subscribe(
                        imageDataListLiveData::setValue,
                        throwable -> imageDataListLiveData.setValue(null)
                ));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
