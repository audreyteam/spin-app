package com.example.spinapp.viewmodel;

import com.example.spinapp.repo.DataRepository;
import com.example.spinapp.utils.SchedulerProvider;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * Factory that creates {@link ViewModel} instances.
 */
public class ViewModelFactory implements ViewModelProvider.Factory {

    private final DataRepository dataRepository;
    private final SchedulerProvider schedulerProvider;

    public ViewModelFactory() {
        this.dataRepository = DataRepository.getInstance();
        this.schedulerProvider = new SchedulerProvider();
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked") // Safe by covariance.
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(ImageDataListViewModel.class)) {
            return (T) new ImageDataListViewModel(dataRepository, schedulerProvider);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
