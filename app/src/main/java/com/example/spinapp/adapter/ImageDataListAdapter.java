package com.example.spinapp.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.spinapp.R;
import com.example.spinapp.model.ImageData;
import com.example.spinapp.utils.ListUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.core.util.Preconditions.checkNotNull;

public class ImageDataListAdapter extends RecyclerView.Adapter<ImageDataListAdapter.DataViewHolder> {
    private final Context context;
    private List<ImageData> imageDataList;

    public ImageDataListAdapter(Context context, List<ImageData> imageDataList) {
        this.context = checkNotNull(context);
        this.imageDataList = checkNotNull(imageDataList);
    }

    public void setImageDataList(List<ImageData> dataList) {
        imageDataList = dataList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        ImageData data = imageDataList.get(position);
        holder.title.setText(data.getTitle());
        Drawable placeholder = new ColorDrawable(ContextCompat.getColor(context, R.color.image_placeholder));
        int radiusCorners = context.getResources().getInteger(R.integer.image_radius_corners);
        RequestOptions requestOptions = new RequestOptions().placeholder(placeholder).error(placeholder).transform(new RoundedCorners(radiusCorners));
        Glide.with(context).load(data.getUrl()).apply(requestOptions).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return ListUtils.size(imageDataList);
    }

    static class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;

        DataViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
