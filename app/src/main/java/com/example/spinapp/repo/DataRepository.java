package com.example.spinapp.repo;

import com.example.spinapp.model.ImageData;
import com.example.spinapp.rest.DataRestApi;
import com.example.spinapp.rest.RestApiFactory;

import java.util.List;

import io.reactivex.Single;

/**
 * Handles data operations and provides a clean API with {@link DataRestApi} to gather data.
 */
public class DataRepository {
    private DataRestApi dataRestApi;

    private DataRepository() {
        dataRestApi = RestApiFactory.createRestApi(DataRestApi.class);
    }

    public static DataRepository getInstance() {
        return DataRepositoryHolder.dataRepository;
    }

    public Single<List<ImageData>> fetchImageDataList() {
        return dataRestApi.getImageDataList().flatMap(dataResponse -> Single.just(dataResponse.getImageDataList()));
    }

    private static class DataRepositoryHolder {
        private static final DataRepository dataRepository = new DataRepository();
    }
}
